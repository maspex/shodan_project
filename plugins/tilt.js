import Vue from 'vue'
import VanillaTilt from '../assets/js/vanilla-tilt.babel.js'

Vue.use(VanillaTilt)
