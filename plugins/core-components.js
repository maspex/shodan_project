import Vue from 'vue'

import PageTitle from '@/components/UI/Titles'
import ArticleList from '@/components/Articles/ArticleList'
import Logo from '@/components/Logo'

Vue.component('page-title', PageTitle)
Vue.component('article-list', ArticleList)
Vue.component('logo', Logo)
