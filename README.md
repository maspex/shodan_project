# Projet AISight

Le projet AISight est une application web universelle basée sur le thème de l'Intelligence Artificielle. Il présente du contenu artistique et technique, créé par des machines. Il se constitue de plusieurs pages, chacune ayant son thème, comme la musique, les images, et l'écriture. L'application permet également l'accès à un jeu, basé sur la caméra de l'utilisateur, qui présentera des vidéos droles tout en examinant les émotions de l'utilisateur.
Le système d'écriture présente des articles écrits par des machines. Le format est celui d'un blog classique. Il est possible d'ajouter des articles, d'en modifier ou d'en supprimer.
Le système de musique présente des pièces musicales composées par des machines, de divers genres, sous la forme de lecteurs Soundcloud intégrés.
Le système d'images est une simple galerie présentant des images artistiques créées ou modifiées par des machines (réseaux de neurones convolutifs)
Un système d'authentification est présent, il permet à un utilisateur de s'enregistrer et d'avoir accès aux fonctions d'administration.
L'application est construite avec NuxtJS, le backend est basé sur Firebase. Le jeu est basé sur l'API face-api.js, elle même basée sur une conversion en javascript du moteur d'apprentissage machine de Google, Tenserflow.


## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).


**Notes**
Les machines intelligentes n'existent désormais plus uniquement dans le domaine de la science-fiction, et nombre d'experts à travers le monde sont divisés par une question: Devrions nous nous en réjouir, ou bien nous alarmer?
Beaucoup pensent encore que la complexité du cerveau humain est inégalée et inégalable... Pourtant elle n'est que le résultat de mutations aléatoires dans le temps. Ainsi, dans certains domaines, les êtres humains ont déjà été dépassés par leur créations. Le nom de l'ex-champion du monde d'échecs Gary Kasparov sera familier a certains, notamment pour sa défaite face à une machine. Pour mettre les choses en perspective, cette défaite s'est produite il y a plus de 20 ans. Les progrès dans le domaine de l'Intelligence Artificielle n'ont pas cessés depuis, et aujourd'hui, les machines dominent l'un des jeux les plus anciens et complexes jamais créé par l'Homme: le Go, avec la victoire en 2016 d'AlphaGo. Pensez-y: plus jamais un être humain ne pourra dépasser le niveau des machines, et devenir meilleur qu'elles.

Il y a heureusement bien plus à la condition humaine que les échecs ou le go, mais après ces constats, pouvons nous réellement penser que les humains pourraient être rendus obsolètes dans d'autres domaine?
Si l'Intelligence Artificielle devenait plus compétente que nous dans tous les domaines, quel utilité aurions nous à ce moment-là? 

Qu'à fait Kasparov après sa défaite? Il a contribué aux avancées de l'Intelligence Artificielle, notamment dans le monde des échecs et du jeu. Il a appris de sa défaite, et a transformé "l'Homme contre la Machine", en "l'Homme + la Machine". Sa théorie est dans le titre de son TED talk: "Don't fear intelligent machines, work with them."
Aujourd'hui, les meilleurs joueurs d'échecs au monde sont des cyborgs, combinaisons de joueurs humains et d'AI, et ils sont presque impossible a battre.
http://www.bbc.com/future/story/20151201-the-cyborg-chess-players-that-cant-be-beaten



### Why Nuxt
**Universal apps**
A universal app is used to describe JavaScript code that can execute both on the client and the server side.
Modern frameworks vue & react can mainly create SPAs, meaning their JS is rendering the html after the initial requests on client side. That means when a crawler is requesting content for referencement, it receives an almost empty html doc, with no content to reference.
UAs allow to have an SPA for smooth UI, but the index.html page is pre rendered server-side, meaning it already has all the content a crawler will need to reference.
It's hard and time consuming to build UAs with regular frameworks, because theres lots of config to do both server and client side
A universal app is about having an SPA, but instead of having a blank index.html page, you’re preloading the application on a web server and sending rendered HTML as the response to a browser request for every route in order to speed up load times and improve SEO by making it easier for Google to crawl the page.


### Server side rendering
Even though Nuxt can pre-render pages on the server, it's not a server side framework, therefore in nuxt vue code, dont directly add code to connect to DB. Possible to access some server side ressources like axios requests.
Typical setup like mine looks like a normal SPA with restful API as backend sending requests to, but can already connect to that backend on the server through axios and nuxt so that we can still get that initial data for pre rendering.
That means that my vue app is now optimized for SEO

### Problem encountered
*API usage*
Api was used in the plugins, and therefore rendered server side before the app was loaded. This API has two render modes, that are auto selected based on the environment it is executed in, browser env & nodejs env. Server side meant the app was executed in a node env, and so I couldn't get it to work normally. Had to use Nuxt workaround to specify to the API it was to run in the client environment

***To grab for pres***
- Résumé en francais (200 mots, suit le plan type RC en page 7 https://drive.google.com/drive/u/1/folders/1Hz4KE9No7c8bPxhXrH02ka6bvtEcJCxA)
- Dossier pro + imprimé en 2
- livrets d'avaluation signé
- dossier projet numérique a rendre avant le 18/09
- Carte ID
- Convocation

**How to fill up dossier pro**
1 Problématique / contexte
2 Développement des fonctionnalités
3 Maquettage et réflection architecture
4 Schéma de données (pourquoi ces choix, quels outils, comment il a été pensé)
5 Fonctionnalités
  SS rendering - 
6 Pourquoi ce projet (intéressant, découvertes techno, quelle partie les plus plaisantes)
7 Conclusion
 Démarche Projet (tâches/étapes/gestion)

**Déroulement Présentation**
Apercu rapide (organisation, fonctionnalités) - 2 mins
PowerPoint - 10 mins
Présentation complète (Fonctionnalités, how it works, détails, APIs, problématiques) - 10 mins
Bonus aurait aimé ajouter - 2 mins
Pourquoi ce projet - 6 mins


### Auth
Using an authentication flow where I'm using middleware and also use vuexStore and other tools like cookies and localstorage to persist login state