// eslint-disable-next-line nuxt/no-cjs-in-config
const bodyParser = require('body-parser') // gotta use require syntax because it'll be executed by node

export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */ // Universal mode, alternative is SPA. SPA doesn't use server side pre rendering, it's just a quick way to build a SPA with vue.
  // Universal uses Nuxt to prerender the views on the server.
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */ // head section is the html header section that's added to every page
  // using it as JS objects, with key/values pairs (name & content)
  // Useful for SEO, social media shareability etc
  head: {
    title: process.env.npm_package_name || 'AISight',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || 'Keeping an Eye on you' }
    ],
    // can also add links
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.2.0/css/all.css' }
    ]
  }, // it's also possibl to override these settings for specific pages (only pages)

  /*
  ** Loading bar color (can disable by setting to false)
  */ // the thing with timing is that this bar doesn't know how long the page takes to load fully, so setting it to a good amount of time might enhance UX, but it usually is done faster than that
  loading: { color: '#831217f6', failedColor: 'yellow', duration: 5000 },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/styles/main.css',
    '~/assets/styles/custom.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    '@plugins/vue-particles.js',
    '@plugins/tilt.js',
    '@plugins/core-components.js',
    '@plugins/date-filter.js'
  ],

  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,

  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    // 'nuxt-buefy',
    ['nuxt-buefy', { css: false, materialDesignIcons: false }],
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },

  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: process.env.BASE_URL || 'https://aisight-29129.firebaseio.com',
    credentials: false
  },
  env: {
    baseURL: process.env.BASE_URL || 'https://aisight-29129.firebaseio.com',
    apiKey: process.env.API_KEY || 'AIzaSyB795IKRaAcjV0ttkpC2tH7uHAzR9g4Kiw'
  },

  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
    transpile: ['vue-particles', 'faceapi.js']
  },

  pageTransition: {
    name: 'fade',
    mode: 'out-in'
  },
  serverMiddleware: [
    bodyParser.json(),
    '@/api/index.js'
  ] // A collection of node & express compatible middleware that will be executed prior to the nuxt rendering process
  // can register any express middleware that needs to be run first
}
