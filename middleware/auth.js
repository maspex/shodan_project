export default function (context) {
  if (!context.store.getters.isLoggedIn) {
    context.redirect('/login')
  }
} // Redirects the user to login page if they try to access admin side
