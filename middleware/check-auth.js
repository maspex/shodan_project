export default function (context) {
  context.store.dispatch('initAuth', context.req)
}
// checking if token is valid by calling initauth on every route that needs a token
