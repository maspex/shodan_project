/* eslint-disable no-console */
import Vuex from 'vuex'
import Cookie from 'js-cookie'

// with a function that returns a new store, every new user receives their own store,
// good for managing user-specific data
const createStore = () => {
  return new Vuex.Store({
    state: {
      loadedArticles: [],
      token: null
    },

    mutations: {
      setArticles (state, articles) {
        state.loadedArticles = articles
      },
      addArticle (state, article) {
        state.loadedArticles.push(article)
      },
      editArticle (state, editedArticle) {
        const articleIndex = state.loadedArticles
          .findIndex(article => article.id === editedArticle.id)
        state.loadedArticles[articleIndex] = editedArticle
      },
      setIDToken (state, token) {
        state.token = token
      },
      clearIDToken (state) {
        state.token = null
      }
    },

    actions: {
      nuxtServerInit (vuexContext, context) {
        return context.app.$axios
          .$get('/articles.json')
          .then((data) => {
            const articlesArray = []
            for (const key in data) {
              articlesArray.push({ ...data[key], id: key })
            }
            vuexContext.commit('setArticles', articlesArray)
          })
          .catch(e => context.error(e))
      },
      addArticle (vuexContext, article) {
        const createdArticle = {
          ...article,
          updatedAt: new Date()
        }
        return this.$axios
          .$post('/articles.json?auth=' + vuexContext.state.token, createdArticle)
          .then((data) => {
            vuexContext.commit('addArticle', { ...createdArticle, id: data.name })
          })
          .catch(err => console.log(err))
      },
      editArticle (vuexContext, editedArticle) {
        // can access editedarticle id because added manually in articleID during fetch from back
        return this.$axios
          .$put(
            '/articles/' +
            editedArticle.id +
            '.json?auth=' +
            vuexContext.state.token, editedArticle)
        // returning to wait for it so finish and handle redirection
          .then(resp =>
            // Updating local store by commiting editArt mutation and passing editedArt
            vuexContext.commit('editArticle', editedArticle)
          )
          .catch(e => console.log(e))
        // Not on the server anymore so can edit live then put modif on server
      },
      setArticles (vuexContext, articles) {
        vuexContext.commit('setArticles', articles)
      },
      authenticateUser (vuexContext, authData) {
        let authUrl =
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=' + process.env.apiKey
        if (!authData.isLogin) {
          authUrl =
          'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=' + process.env.apiKey
        }
        return this.$axios
          .$post(authUrl, {
            email: authData.email,
            password: authData.password,
            returnSecureToken: true
          })
          .then((res) => {
            console.log(res)
            vuexContext.commit('setIDToken', res.idToken)// idToken is the name of the prop that holds the token
            localStorage.setItem('token', res.idToken) // Setting the token in locastorage
            localStorage.setItem('tokenExpiration',
              new Date().getTime() + Number.parseInt(res.expiresIn) * 1000) // duration is in miliseconds, firebase timer is in seconds, so gotta scale up setimeout
            Cookie.set('idCookie', res.idToken) // Setting the cookie to token
            Cookie.set('expirationDate',
              new Date().getTime() + Number.parseInt(res.expiresIn) * 1000)
            return this.$axios.$post('http://localhost:3000/api/track-auth', { data: 'User authenticated' })
          })
          .catch(e => console.log(e))
      },
      initAuth (vuexContext, req) {
        // Checking localstorage and cookies for IDtoken, then storing it in var, will either be undefined or the last stored token
        let token
        let expirationDate
        if (req) {
          if (!req.headers.cookie) {
            return
          }
          const idCookie = req.headers.cookie
            .split(';')
            .find(c => c.trim().startsWith('idCookie='))
          if (!idCookie) {
            return
          }
          // cookies are attached to every get request to serv, so can get them from there
          token = idCookie.split('=')[1]
          expirationDate = req.headers.cookie
            .split(';') // splitting by the semicolon, accessing the cookie
            .find(c => c.trim().startsWith('expirationDate=')) // searching by the key used when I set the cookie
            .split('=')[1] // splitting by equal sign, and get second value in remaining array, which is the actual date
        } else { // else if in the client on SPA, can access localStorage to get the token
          token = localStorage.getItem('token')
          expirationDate = localStorage.getItem('tokenExpiration')
        }
        // if find out there's no expiration date for token
        console.log('No token or invalid token')
        if (new Date().getTime() > +expirationDate || !token) { // +expirationDate converts string to number
          vuexContext.dispatch('logout') // then dispatch logout function
          return
        }
        vuexContext.commit('setIDToken', token)
      },
      logout (vuexContext) {
        vuexContext.commit('clearIDToken')
        Cookie.remove('idCookie')
        Cookie.remove('expirationDate')
        if (process.client) {
          localStorage.removeItem('token')
          localStorage.removeItem('tokenExpiration')
        } // env check to make sure it's safe to access the client side API localstorage
      }
    },

    getters: {
      loadedArticles (state) {
        return state.loadedArticles
      },
      // Checking if user is logged in
      isLoggedIn (state) {
        return state.token != null
      }
    }
  })
}

// With this store, I can add & manipulate data, AND update it locally to always reflect the latest state,
// even without fetching from server
export default createStore
