/* eslint-disable no-console */
// Express integration to use custom server side code
const express = require('express')
const router = express.Router()

const app = express() // Instantiate express app
router.use((req, res, next) => { // Take incoming request & response data
  Object.setPrototypeOf(req, app.request) // then map it to the express API
  Object.setPrototypeOf(res, app.response)
  req.res = res
  res.req = req
  next()
})

// creating custom router here
router.post('/track-auth', (req, res) => {
  // default express route to get request & response
  // then, can execute any server side code that needs to be rendered prior to nuxt rendering
  // Here I could import mongoose and connect to my own DB
  console.log('Stored Data', req.body.data)
  res.statusCode(200).json({ message: 'Success!' })
})

module.exports = {
  path: '/api',
  handler: router
}
